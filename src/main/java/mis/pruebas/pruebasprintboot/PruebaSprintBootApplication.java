package mis.pruebas.pruebasprintboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaSprintBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaSprintBootApplication.class, args);
	}

}
