package mis.pruebas.pruebasprintboot.controladores;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
@RequestMapping("/hola/v1")
public class ControladorHola {

    String saludo = "Hola";

    @ GetMapping
    public String saludar () {
        return this.saludo;
    }
    @PutMapping
    public void modificarSaludo(String saludoNuevo) {
        System.out.println(String.format("saludo modificado : %S", saludoNuevo));
                this.saludo = saludoNuevo;
    }
}
